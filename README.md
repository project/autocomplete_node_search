    Welcome to Autocomplete Node search.

    If you're having trouble installing this module, please ensure that your
    tar program is not flattening the directory tree, truncating filenames
    or losing files.

    Installing Autocomplete Node search:

    Module Autocomplete Node search will provide you a block 
    which you can configure in any region and search any node title.

    After hitting enter it will get redirected to the searched page.

    You can find this block by navigating through `admin/structure/block`.

    If you are configuring `Autocomplete Node search` for anonymous users, 
    permission should be given for anonymous users.

    If your search yields no result, no redirection will take place. 
    Hitting enter/submit button will give you back the existence page 
    on which you were earlier.

    Autocomplete Node Search administration pages

    Autocomplete node search
    Configure Autocomplete Node Search permissions
